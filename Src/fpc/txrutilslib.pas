unit TxRUtilslib;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,TxRTokenLib, TxRTokenItem,TxRTypeslib;

const
  WHITESPACE_CHARS:pchar = '\[]()=';

//function isDispleyed(c:char):boolean;
function isWhitespace(c:char):boolean;
function isSysText(c:char):boolean;

type
 { TxRTokenList }
  PTxRToken = ^TxRToken;

  TxRTokenList = class
    private
      Head:PTokenItem;
      ListStart:PTokenItem;
      itr:PTokenItem;
      loop:boolean;
      Count:integer;
    public
      constructor Create;
      destructor  Destroy;

      procedure Add(i:TxRTokenType;val:string);
      procedure StartIter;
      function getToken:PTxRToken;
      function isNoEOL:boolean;
      procedure NextToken;

  end;

implementation

{function isDispleyed(c: pchar): boolean;
begin
   if (ord(c) > 31) and (ord(c)<255) then
      result:=true
   else
     result:=false;
end;
 }

function isWhitespace(c: char): boolean;
begin
  if c in [' ',#10,#13] then
     result:=true
  else
    result:=false
end;

function isSysText(c: char): boolean;
begin
  if c in ['\','[',']','='] then result:=true
  else result:=false;
end;

{ TxRListToken }

constructor TxRTokenList.Create;
begin
  writeln('--- CreateTokenList ---');
  self.Head:=nil;
  self.ListStart:=nil;
  self.Count:=0;
  self.itr:=nil;
  self.loop:=false;
end;

destructor TxRTokenList.Destroy;
begin

end;

procedure TxRTokenList.Add(i:TxRTokenType;val:string);
var
  node: PTokenItem;
  token:PTxRToken;
begin
  new(token);
  token^.Create(i,val);

  new(node);
  node^.Item := token;
  node^.Next := nil;

  if self.Head = nil then
  begin
    self.ListStart:=node;
    self.Head:=node;
  end
  else
  begin
    self.Head^.Next:=node;
    self.Head := node;
  end;
  inc(self.Count);
end;

procedure TxRTokenList.StartIter;
begin
  self.itr:=self.ListStart;
  self.loop:=true;
end;

function TxRTokenList.getToken: PTxRToken;
begin
   result:=self.itr^.Item;
end;

function TxRTokenList.isNoEOL: boolean;
begin
   result:=self.loop;
end;

procedure TxRTokenList.NextToken;
begin
  if self.loop then
  begin
    if self.itr^.Next=nil then
    begin
      self.loop:=false;
    end
    else
      begin
        self.itr:=self.itr^.Next;
      end;
  end;
end;

end.

