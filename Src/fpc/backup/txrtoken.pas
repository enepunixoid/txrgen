unit TxRToken;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,TxRTokenType;
type

  { txrToken }

  _txrToken = class
    private
      tType:txrTokenTypes;
      tStr:pchar;
    public


      constructor Create(tt:txrTokenTypes; ts:pchar);
      destructor Destroy;
      function getType:_txrToken;
      procedure setType(tt:_txrToken);
      function getText:pchar;
      procedure setText(s:pchar);
      function getEOS:boolean;
  end;



implementation

{ txrToken }

constructor _txrToken.Create(tt: txrTokenTypes; ts: pchar);
begin
  self.tType := tt;
  self.tStr := ts;
end;

destructor _txrToken.Destroy;
begin
   self.tStr:='';
end;
                                        c
function _txrToken.getType: _txrToken:_TxRTokenTypes;
begin
   result:=self.tType;
end;

procedure _txrToken.setType(tt: _txrToken);
begin
   self.tType:= tt;
end;

function _txrToken.getText: pchar;
begin
  result:=self.tStr;
end;

procedure _txrToken.setText(s: pchar);
begin
   self.tStr:=s;
end;

end.

