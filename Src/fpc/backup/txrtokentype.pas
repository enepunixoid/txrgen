unit TxRTokenType;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;
type
  TxRTokenType = (
     TEXT,      {'0'..'9','.',Letters from the alphabet}
     BACKSLASH,     {'\'}
     LSB,           {left square bracket '[' }
     RSB,           {right square bracket ']'}
     PERSENT        {Persent '%'}
     );

implementation

end.

