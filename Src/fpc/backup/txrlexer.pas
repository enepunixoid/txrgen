unit TxRLexer;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,TxRListToken,TxRToken;
type

{ txrLexer }

 txrLexer = class
     private
       input:pchar;
       tokens:txrListTokens;
       __pos:integer;
       len:integer;
       function isServiceSym(c:char):boolean;
       function isNotPrinting(c:char):boolean;
     public
       const
           OPERATOR_CHARS='\%[]';
           OPERATOR_TOKENS: array of TokenType = (
             TokenType.BACKSLASH,TokenType.PERSENT,TokenType.LSB,TokenType.RSB
             );
       constructor Create(i:pchar);
       function peek(rp:integer):char;
       function next:char;
       function Tokenize:txrListTokens;
       function TokenizeText:txrLsitTokens;
       function TokenSysSymbols:txrListTokens;
       procedure addToken(tt:trxTokenType; t:pchar='');

     end;


implementation

{ txrLexer }

function txrLexer.isSrviceSym(c: char): boolean;
var
  i:integer;
begin
  for i:=0 to Lenght(self.OPERATOR_CHARS) do
  begin
      if self.OPERATOR_CHARS[i] = c then result:=true;
  end;
  result:=false;

end;

function txrLexer.isNotPrinting(c:char):boolean;
begin
  if ord(c)>0 and ord(c)<32 then result:=true;
  else result:=false;
end;

constructor txrLexer.Create(i: pchar);
begin
  self.input = i;
  self.len:=Lenght(i);
  self._pos:=0;
  self.tokens:=txrListTokens.Create;
end;

function txrLexer.peek(rp: integer): char;
var p:integer;
begin
   p := self._pos + rp;
   if p>self.len then result='';
   else result:=self.input[p];
end;

function txrLexer.next: char;
begin
  inc(self._pos);
  result:=self.peek(0);
end;

function txrLexer.Tokenize: txrListTokens;
var
  cur:char;
begin
   while self.pos < self.len do
   begin
     cur:=self.peek(0);
     if c = '\' then self.TokenDirecvive
     else if pos(c, ' ') then self.next;
     else self.TokenizeText;
   end;
end;

function txrLexer.TokenizeText: txrLsitTokens;
var
  buf:string;
  c:char;
begin
   c:=peek(0);
   while c<>self.isServiceSym(c) or  do
   begin
         buf:=buf+c;
         c:=next();
   end;
   self.addToken(TokenType.TEXT,buf);

end;

function txrLexer.TokenSysSymbols: txrListTokens;
var
  i:integer
begin
  self.addToken(self.OPERATOR_TOKENS[pos(self.OPERATOR_CHARS,peek(0))]);
  self.next;

end;

procedure txrLexer.addToken(tt: trxTokenType; t: pchar);
begin
   self.tokens.addToken(txrToken.Create(tt,t));
end;

end.

