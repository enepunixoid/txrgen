unit TxRLexerLib;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  TxRUtilslib,TxRTokenLib,TxRTypeslib;

type

{ TxRLexer }

 TxRLexer = class
 const
      SERVICE_CHARS: pchar = '\[]=%';
      SERVICE_TOKENS:array of TxRTokenType = (
            TxRTokenType.DIRECTIVE,TxRTokenType.LSB,
            TxRTokenType.RSB,TxRTokenType.EQ,
            TxRTokenType.PROCENT
         );
 private
    Input:pchar;
    Tokens: TxRTokenList;
    PosInInput:integer;

    procedure AddToken(tt:TxRTokenType;c:string='');
    function peek(rp:integer):char;
    function next():char;
  public
    constructor Create(i:pchar);

    function Tokenize:TxRTokenList;
    procedure TokenizeDirective();
    procedure TokenizeText();

    function getTokensAsString:string;
  end;
implementation

{ TxRLexer }

procedure TxRLexer.AddToken(tt: TxRTokenType; c: string);
begin
  self.Tokens.Add(tt,c);
end;

function TxRLexer.peek(rp: integer): char;
var pp: integer;
begin
   pp:=self.PosInInput + rp;
   if self.Input[pp] = #0 then result:=#0
   else result:= self.Input[pp];
end;

function TxRLexer.next: char;
begin
   inc(self.PosInInput);
   result:=self.peek(0);
end;

constructor TxRLexer.Create(i: pchar);
begin
   writeln('Create Lexer Class');
  self.Input := i;
  self.Tokens:=TxRTokenList.Create;
end;



function TxRLexer.Tokenize: TxRTokenList;
var
   current:char;
begin
  while self.Input[self.PosInInput] <> chr(0) do
  begin
       current:=self.peek(0);
       if current = '\' then self.TokenizeDirective
       else if current in [' ',#13,#10] then self.next
       else TokenizeText;

  end;
  result:=self.Tokens;
end;

procedure TxRLexer.TokenizeDirective;
var
   buf:string;
   cur:char;
begin
     buf:='';
     cur:=self.next;
     while TxRUtilslib.isWhitespace(cur) = false do
     begin
          buf := buf + cur;
          cur := self.next;
     end;
     writeln('D+('+buf+')');
     self.AddToken(TxRTokenType.DIRECTIVE,buf);



end;

procedure TxRLexer.TokenizeText;
var
   buf:string;
   cur:char;
begin
  buf:='';
  cur:=self.peek(0);
  while  TxRUtilsLib.isSysText(cur) = false do
  begin
     buf := buf+cur;
     cur := self.next;
  end;
  writeln('T+('+buf+')');
  self.AddToken(TxRTokenType.TEXT,buf);
end;

function TxRLexer.getTokensAsString: string;
var
   t:PTxRToken;

   buf:string;
begin
 self.Tokens.StartIter;
 while self.Tokens.isNoEOL do
 begin
    t:=self.Tokens.getToken;
    buf:= buf +' '+t^.getNameTypeAndValue;
    write('.');
    self.Tokens.NextToken;

 end;
 writeln(buf);
 result:=buf;
end;

end.

