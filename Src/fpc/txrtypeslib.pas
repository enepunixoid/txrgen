unit TxRTypeslib;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

type
  {$packEnum 1}
  TxRTokenType = (
     DIRECTIVE = 1, {'/'}
     TEXT      = 2,
     PROCENT   = 3, {'%'}
     LSB       = 4,
     RSB       = 5,
     EQ        = 6
  );

implementation

end.

