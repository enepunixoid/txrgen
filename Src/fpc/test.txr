\txr
\head Text
\p The type \b TextFile\n (or, equivalent and older, just Text) is used in a Pascal program to read from and write to a text file.
\p The variable representing the text file (MyFile in the example above) may be used to read from, write to, or both to the actual file. It must be tied to the actual file by a run-time library routine AssignFile. Then the file must be opened by the Reset, Rewrite or Append procedure. You can read and write to file using Read, Readln, Write, Writeln. After you have finished processing the file, you should release the necessary file resources by closing the file calling CloseFile.
\code
var
  MyFile: TextFile;
  s: string;
begin
  AssignFile(MyFile, 'a.txt');

  try
    reset(MyFile);    //Reopen the file for reading
    readln(MyFile, s);
    writeln('Text read from file: ', s)

    {
    or add some text:
    append(MyFile);
    writeln(MyFile, 'some text');
    }

  finally
    CloseFile(MyFile)
  end
end.
\p The variable representing the text file (MyFile in the example above) may be used to read from, write to, or both to the actual file. It must be tied to the actual file by a run-time library routine AssignFile. Then the file must be opened by the Reset, Rewrite or Append procedure. You can read and write to file using Read, Readln, Write, Writeln. After you have finished processing the file, you should release the necessary file resources by closing the file calling CloseFile.

