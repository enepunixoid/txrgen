unit TxRTokenLib;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,TxRTypeslib;
type

  { TxRToken }

  TxRToken = object
    const
      NameTokens:array of string = ('DIRECTIVE','TEXT');
    private
      TokenType:TxRTokenType;
      Text:string;
    public

      constructor Create(tt:TxRTokenType;t:String);
      destructor Destroy;
      function getText:string;
      function getType:TxRTokenType;
      procedure setText(t:string);
      procedure setType(tt:TxRTokenType);
      function getNameTypeAndValue:string;


  end;
  PTxRToken = ^TxRToken;

implementation

{ TxRToken }

constructor TxRToken.Create(tt: TxRTokenType; t: string);
begin

  self.TokenType:=tt;
  self.Text:=t;

   inherited;
end;

destructor TxRToken.Destroy;
begin
   //delete(self.Text,1,Length(self.Text));
   inherited;

end;

function TxRToken.getText: string;
begin
  result:=self.Text;
end;

function TxRToken.getType: TxRTokenType;
begin
  result:=self.TokenType;
end;

procedure TxRToken.setText(t: string);
begin
   self.Text:=t;
end;

procedure TxRToken.setType(tt: TxRTokenType);
begin
   self.TokenType:=tt;
end;

function TxRToken.getNameTypeAndValue: string;

begin
   //i:= self.TokenType;
  result:= self.NameTokens[qword(self.TokenType)] + '('+self.Text+')';
end;



end.

