# Descriptopn TxR



## Description syntax BNF
```
<TXR>         ::= <DIRECTIVE> | <DIRECTIVE> <TXR>
<DIRECTIVE>   ::= <DIR> | <DIR> <TEXT> 
<DIR>         ::= "\" <INDEBTIFIER> | "\" <INDEBTIFIER> "[" <PARAMS> "]"
<PARAMS>      ::= <PARAM> | <PARAM> <PARAMS>
<PARAM>       ::= <INDEBTIFIER> "=" <VALUE>
<VALUE>       ::= <VAL> | <VAL> <VALUE>
<VAL>         ::=<ALPHABET> | <DIGITEL> | "_" 
<INDEBTIFIER> ::= <ALPHABET> |  <ALPHABET> <INDEBTIFIER>

```
Members of some indentifier

- ALPHABET - Latin Alphabet
- DIGITAL  - Digital symbols "1","2","3","4","5","6","7","8","9","10"
- TEXT     - All displayed and printing symbols


## Examle TxR Template

```
\TxR
\documet[size=A4]
\margin[top=1px,bottom=1px,left=1px,right=1px]
\padding[top=1px,bottom=1px,left=1px,right=1px]
\bc=#ffffff
\color=#000000
\page
\head Examle TxR Template file
\parag \b Lorem\t Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.


```

- ```\TxR``` - Required initial tag indicating the beginning of the template.
- ```\document``` - Document page ```size``` equal to A0,A1,A2,A3,A4,A5 or A6. Set by default for all document pages. Default size A4. 
- ```\margin``` - This directive establishes indentation one pages of documet. top - along to top edge, margin - along the bottom edge, left - along the left edge and right - along the ringht edge. 
